# QtWorkshop
This repository serves as a set of Qt exercises for practicing Qt, C++, and object oriented
development.

## Exercises
This is a list of the exercises in the order they are intended to be completed in. Details for
each exercise can be found in its folders README.
- Exercise 1: FizzBuzz
