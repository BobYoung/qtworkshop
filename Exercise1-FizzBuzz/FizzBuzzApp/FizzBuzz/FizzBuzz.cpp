#include "FizzBuzz.h"
#include <QStringList>
#include <iostream>

FizzBuzz::FizzBuzz(MainWindowInterface& window)
    : Window(&window)
{
    QObject::connect(Window, &MainWindowInterface::CalculateButtonClicked, this, &FizzBuzz::OnCalculateButtonClicked);
}

void FizzBuzz::Run()
{
    Window->show();
}

void FizzBuzz::OnCalculateButtonClicked(const int count)
{
    Q_UNUSED(count)
    // TODO: Implement FizzBuzz kata and display resulting string to Window
}
